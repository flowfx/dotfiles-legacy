" FZF-specific options

" Better buffers with FZF https://statico.github.io/vim3.html
nmap ; :Buffers<CR>

" Rails-specific FZF mappings
" https://www.destroyallsoftware.com/file-navigation-in-vim.html
command! -bang ControllerFiles call fzf#vim#files('app/controllers', <bang>0)
command! -bang ModelFiles call fzf#vim#files('app/models', <bang>0)
command! -bang ViewFiles call fzf#vim#files('app/views', <bang>0)
nmap <localleader>gc :ControllerFiles<CR>
nmap <localleader>gm :ModelFiles<CR>
nmap <localleader>gv :ViewFiles<CR>

" https://github.com/junegunn/fzf/blob/master/README-VIM.md#examples
" let g:fzf_layout = { 'down': '60%' }
let g:fzf_layout = { 'window': { 'width': 0.95, 'height': 0.6, 'relative': v:true, 'yoffset': 1.0 } }

" make FZF respect gitignore if `ag` is installed
if (executable('ag'))
  let $FZF_DEFAULT_COMMAND = 'ag --hidden --ignore .git -g ""'
endif

let g:fzf_action = {
  \ 'ctrl-t': 'tab split',
  \ 'ctrl-s': 'split',
  \ 'ctrl-v': 'vsplit' }

""" ACK & AG (The Silver Searcher) """
if executable('ag')
  let g:ackprg = 'ag --vimgrep'
endif

" cnoreabbrev Ack Ack1
nnoremap <LocalLeader>a :Ack!<Space>
" nmap <localleader>a :Rg<cr>
