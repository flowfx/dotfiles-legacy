My dotfiles, based on [thoughtbot's dotfiles](https://github.com/thoughtbot/dotfiles/) and
[rcm](https://github.com/thoughtbot/rcm).

Cf. the readme for my [Ansible playbook](https://github.com/flowfx/ansible-localhost) for my laptops.


## More

For Vim
```
brew install fzf the_silver_searcher
sudo apt install fzf silversearcher-ag
```

General
```
brew install tree tig tmuxinator ctags hub markdown pandoc
sudo apt install tree tig tmuxinator exuberant-ctags hub markdown pandoc 

# audo foo
brew install ffmpeg lame flac mplayer
```
